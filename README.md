#Java
##Usage##
##
Required Arguments: File and Number

Run by building Jar - Import Project as Gradle Project and Click on **Gradle->Tasks->Build->Jar** :: Jar File named result.jar will be built and saved in **CurrentProject/build/libs/** directory. Or run the already generated jar file - Present inside folder **generated_jar**

java -jar result.jar -f C:\Music\file.log -n 8

or 

java -jar result.jar -f=C:\Music\file.log -n=8

or 

java -jar result.jar -f C:\Music\file.log -n=8


---
###Sample Result###

![Alt text](https://i.ibb.co/2Nzsh9q/result.png)